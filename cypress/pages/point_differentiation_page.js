export class PointDifferentiationPage{

    elements = {
        url: () => 'https://gbh.com.do/point-of-differentiation/',
        gbh_diference_h3: () => cy.get('.difference-item__circle--bottom > svg')    
    }

    click_gbh_diference_h3(){
        this.elements.gbh_diference_h3().scrollIntoView().click()       
    }
}