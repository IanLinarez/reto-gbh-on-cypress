export class ITBusinessTransformationPage{

    elements = {
        url: () => 'https://gbh.com.do/vcio/',
        tech_enable_h2: () => cy.get('.et_pb_section_0 > .et_pb_row > .et_pb_column > .et_pb_text > .et_pb_text_inner > h2')
    }
}