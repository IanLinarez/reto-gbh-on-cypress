import "cypress-real-events/support";

export class GbhHomePage{

  elements = {
        increase_impact_btn: () => cy.contains('Let’s increase your impact'), 
        solutions_a: () => cy.get('li[id=menu-item-3240]'),
        innovation_squad_span: () => cy.contains('Innovation Squad'), 
        itbusiness_span: () => cy.contains('IT Business Transformation'), 
        custom_software_span: () => cy.contains('Custom Software Development'), 
        staff_aumentation_span: () => cy.contains('Agent of Change Staff Augmentation'), 
        it_support_service_span: () => cy.contains('IT Support Service'), 
        it_foundation_span: () => cy.contains('IT Foundation'), 
        it_protection_supp_span: () => cy.contains('IT Protection & Support'), 
        compare_products_span: () => cy.contains('Compare products'), 
        it_net_solutions_span: () => cy.contains('IT Network Solutions'), 
        who_we_are_a: () => cy.get('li[id=menu-item-3241]'),
        about_us_span: () => cy.contains('About us'), 
        point_differentiation_span: () => cy.contains('Point Of Differentiation'), 
        blog_span: () => cy.contains('Blog'), 
        case_studies_a: () => cy.get('li[id=menu-item-353]'),
        careers_a: () => cy.get('li[id=menu-item-475]'),
        url: () => 'https://gbh.com.do'
    }
    
  navigate(){
    cy.visit(this.elements.url())
  }
  
  hover_solutions_a(){
    cy.wait(3000)
    this.elements.solutions_a().realHover()
    cy.screenshot("hover_solutions_a",{ capture: "viewport" })
  }
   
  hover_who_we_are_a(){
    cy.wait(3000)
    this.elements.who_we_are_a().realHover()
    cy.screenshot("hover_solutions_a",{ capture: "viewport" })
  }

  click_innovation_squad_span(){
    this.elements.innovation_squad_span().click()
    cy.screenshot("click_innovation_squad_span",{ capture: "viewport" })
  }

  click_itbusiness_span(){
    this.elements.itbusiness_span().click()
    cy.screenshot("click_itbusiness_span",{ capture: "viewport" })
  }

  click_custom_software_span(){
    this.elements.custom_software_span().click()
    cy.screenshot("click_custom_software_span",{ capture: "viewport" })
  }

  click_staff_aumentation_span(){
    this.elements.staff_aumentation_span().click()
    cy.screenshot("click_staff_aumentation_span",{ capture: "viewport" })
  }

  click_it_foundation_span(){
    this.elements.it_support_service_span().click()
    this.elements.it_foundation_span().click()
    cy.screenshot("click_it_foundation_span",{ capture: "viewport" })
  }

  click_it_protection_supp_span(){
    this.elements.it_support_service_span().click()
    this.elements.it_protection_supp_span().click()
    cy.screenshot("it_protection_supp_span",{ capture: "viewport" }) 
  }

  click_compare_products_span(){
    this.elements.it_support_service_span().click()
    this.elements.compare_products_span().click()
    cy.screenshot("click_compare_products_span",{ capture: "viewport" })  
  }

  click_it_net_solutions_span(){
    this.elements.it_net_solutions_span().click()
    cy.screenshot("click_it_net_solutions_span",{ capture: "viewport" })
  }

  click_about_us_span(){
    this.elements.about_us_span().click()
    cy.screenshot("click_about_us_span",{ capture: "viewport" })
  }

  click_point_differentiation_span(){
    this.elements.point_differentiation_span().click()
    cy.screenshot("click_point_differentiation_span",{ capture: "viewport" })
  }

  click_blog_span(){
    this.elements.blog_span().click()
    cy.screenshot("click_blog_span",{ capture: "viewport" })
  }

  click_case_studies_a(){
    this.elements.case_studies_a().click()
    cy.screenshot("click_case_studies_a",{ capture: "viewport" })
  }
  click_careers_a(){
    this.elements.careers_a().click()
    cy.screenshot("click_careers_a",{ capture: "viewport" })
  }
}