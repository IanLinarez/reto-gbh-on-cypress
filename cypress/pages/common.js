///This class contains all common elements and methods 
export class Common{

    elements = {
        option_title_h2: () => cy.get('.et_pb_text_0 > .et_pb_text_inner > h2')
    }
    
    click_option_title_h2(){
        this.elements.option_title_h2().click()
    }
}