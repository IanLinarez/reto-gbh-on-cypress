/// <reference types="cypress" />

import {Common} from "../../pages/common"
import {GbhHomePage} from "../../pages/gbh_homepage"
import {InnovationSquadPage} from "../../pages/innovation_squad_page"
import {ITBusinessTransformationPage} from "../../pages/it_business_transformation_page"
import {CustomSoftwareDevPage} from "../../pages/custom_software_development_page"
import {StaffAumentationPage} from "../../pages/staff_aumentation_page"
import {ITFoundationPage} from "../../pages/it_foundation_page"
import {ITProtectionSupportPage} from "../../pages/it_protection_support_page"
import {CompareProductsPage} from "../../pages/compare_products_page"
import {ITNetSolutionsPage} from "../../pages/it_net_solutions_page"
import {AboutUsPage} from "../../pages/about_us_page"
import {BlogPage} from "../../pages/blog_page"
import {PointDifferentiationPage} from "../../pages/point_differentiation_page"
import {CaseStudiePage} from "../../pages/case_studies_page"
import {CareersPage} from "../../pages/careers_page"

describe('Test Suite: Access pages', () => {
  const gbhhomepage_page = new GbhHomePage()
  const innovationsquad_page = new InnovationSquadPage()
  const itbutr_page = new ITBusinessTransformationPage()
  const customsoftwaredev_page = new CustomSoftwareDevPage()
  const staff_aumentation_page = new StaffAumentationPage()
  const it_foundation_page = new ITFoundationPage()
  const it_protection_supp_page = new ITProtectionSupportPage()
  const compare_products_page = new CompareProductsPage()
  const it_net_solutions_page = new ITNetSolutionsPage()
  const blog_page = new BlogPage()
  const about_us_page = new AboutUsPage()
  const point_differentiation_page = new PointDifferentiationPage()
  const case_studies_page = new CaseStudiePage()
  const careers_page = new CareersPage()
  const common_page = new Common()

  beforeEach(() => { 
    gbhhomepage_page.navigate()
    cy.on('uncaught:exception', (err, runnable) => {
    return false;
    });
  })
  
  it('Validate that all the pages can be entered', () => {
    ///Validate that the button "Let’s increase your impact" is shown on the screen
    gbhhomepage_page.elements.increase_impact_btn().should('be.visible')

    gbhhomepage_page.hover_solutions_a()
    gbhhomepage_page.click_innovation_squad_span()

    //Validate that it redirect to the innovation squad page 
    cy.url().should('include',innovationsquad_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')

    gbhhomepage_page.hover_solutions_a()
    gbhhomepage_page.click_itbusiness_span()

    //Validate that it redirect to the IT business transformation page 
    cy.url().should('include',itbutr_page.elements.url())
    itbutr_page.elements.tech_enable_h2().should('be.visible')

    gbhhomepage_page.hover_solutions_a()
    gbhhomepage_page.click_custom_software_span()

    //Validate that it redirect to the Custom Software Dev page 
    cy.url().should('include',customsoftwaredev_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')

    gbhhomepage_page.hover_solutions_a()
    gbhhomepage_page.click_staff_aumentation_span()

    //Validate that it redirect to the Staff Aumentation page 
    cy.url().should('include',staff_aumentation_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')

    gbhhomepage_page.hover_solutions_a()
    gbhhomepage_page.click_it_foundation_span()

    //Validate that it redirect to the It Foundation page 
    cy.url().should('include',it_foundation_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')

    gbhhomepage_page.hover_solutions_a()
    gbhhomepage_page.click_it_protection_supp_span()

    //Validate that it redirect to the It Protection support page 
    cy.url().should('include',it_protection_supp_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')
  
    gbhhomepage_page.hover_solutions_a()
    gbhhomepage_page.click_compare_products_span()

    //Validate that it redirect to the Compare products page 
    cy.url().should('include',compare_products_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')
  
    gbhhomepage_page.hover_solutions_a()
    gbhhomepage_page.click_it_net_solutions_span()

    //Validate that it redirect to the IT network solutions page 
    cy.url().should('include',it_net_solutions_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')

    gbhhomepage_page.hover_who_we_are_a()
    gbhhomepage_page.click_about_us_span()

    //Validate that it redirect to the About us page 
    cy.url().should('include',about_us_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')

    gbhhomepage_page.hover_who_we_are_a()
    gbhhomepage_page.click_point_differentiation_span()

    //Validate that it redirect to the Point of differentation page 
    cy.url().should('include',point_differentiation_page.elements.url())
    point_differentiation_page.click_gbh_diference_h3()

    gbhhomepage_page.hover_who_we_are_a()
    gbhhomepage_page.click_blog_span()

    //Validate that it redirect to the Blog page 
    cy.url().should('include',blog_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')

    gbhhomepage_page.click_case_studies_a()

    //Validate that it redirect to the Case Studies page 
    cy.url().should('include',case_studies_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')

    gbhhomepage_page.click_careers_a()

    //Validate that it redirect to the Careers page 
    cy.url().should('include',careers_page.elements.url())
    common_page.elements.option_title_h2().should('be.visible')
  })  
})