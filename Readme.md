## Description

Project dedicated for automated test for the GBH page

## Tecnical information

* __System Operation__: Windows 10
* __System Type__: 64-bit
* __Text editor__: Microsoft Visual Studio Code

## Requirements

* [Python 3.8.6](https://www.python.org/ftp/python/3.8.6/python-3.8.6-amd64.exe)
* Library
    * cypress
    * cypress-real-events

## Instalation

use [npm] to install the module [cypress].

```
npm install cypress 
npm install cypress-real-events 

```

To run in chrome headless

npx cypress run --headless